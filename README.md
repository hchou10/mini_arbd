This is my mini-arbd simulation package. The rigid-body is described as a collection of particles which do not have relative motion.
I also add a test directory containing an example of how to simulate a system with thousands of proteins in NPC-like system.
cd test/npc_proteins/;
ln -s ../../mini_arbd;
./mini_arbd min_arbd.bd output > log &
A trajectory named output.dcd will be generated with a log file named log.